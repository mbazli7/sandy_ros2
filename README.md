# Getting Started with ROS2
This is project for getting started and play with ROS2 functionality, features etc. with guidance from [ROS2 For Beginners](https://www.udemy.com/share/103tOU3@lW8Fq92Eq9nFie1vVFT8fLQXLejYAD0SlO6M2booYjwHY9tZ3gnUotrHNHJblG3xRg==/).

# Folder Structure

```
.
├── README.md                       <-- this file
└── src
    ├── my_cpp_pkg
    │   ├── CMakeLists.txt          <-- Cmake file for compilation
    │   ├── include
    │   ├── package.xml             <-- update dependencies here
    │   └── src                     <-- C++ source code
    ├── my_py_pkg
    │   ├── my_py_pkg               <-- Python source code
    │   ├── package.xml             <-- update dependencies here
    │   ├── resource
    │   ├── setup.cfg
    │   ├── setup.py                <-- register package and node here
    │   └── test                    <-- unit test
    └── template
        ├── ros_node_template.cpp
        └── ros_node_template.py
```

# Installation
Below are pre-requisite installation to be done:  

1. Install ROS2 for Ubuntu 20.04 as described [HERE](https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Binary.html)
2. Install `Colcon` package builder by `sudo apt install python3-colcon-common-extensions`
3. Append below lines in `~/.bashrc`  

```
# ROS: source colcon argscomplete
source /usr/share/colcon_argcomplete/hook/colcon-argcomplete.bash
```

4. Source ROS2 and this project environment with `source ./setup.sh`
5. Inside `sandy_ros2` folder, run command `colcon build`
6. Validate installation using command `ros2 run my_cpp_pkg cpp_node`
7. Result should be appeared similarly as below

```
$ ros2 run my_cpp_pkg cpp_node 
[INFO] [1634072483.011747283] [cpp_test]: Hello ROS2 from C++
[INFO] [1634072484.014240414] [cpp_test]: timer_callback 1
[INFO] [1634072485.016403841] [cpp_test]: timer_callback 2
[INFO] [1634072486.018759908] [cpp_test]: timer_callback 3
```