#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from functools import partial

from example_interfaces.srv import AddTwoInts


class AddTwoIntsClientNode(Node):
    def __init__(self):
        super().__init__("add_two_ints_client")
        self.get_logger().info("Add two ints client has been started.")

        # test the server-client communication
        self.call_add_two_ints_server(10, 20)
        self.call_add_two_ints_server(30, 40)
        self.call_add_two_ints_server(50, 60)

    def call_add_two_ints_server(self, a, b):
        # create client and wait for server for ready
        client = self.create_client(AddTwoInts, "add_two_ints")
        while not client.wait_for_service(1.0):
            self.get_logger().warn("Waiting for server Add two ints..")

        # prepare the inputs
        request = AddTwoInts.Request()
        request.a = a
        request.b = b

        # dont wait for response from server
        future = client.call_async(request)
        # partial function is to consolidate more inputs into callback
        future.add_done_callback(partial(self.add_two_ints_callback, a=a, b=b))

    def add_two_ints_callback(self, future, a, b):
        try:
            response = future.result()
            self.get_logger().info("{} + {} = {}".format(a, b, int(response.sum)))
        except Exception as err:
            self.get_logger().error("Service call failed %r" % (err,))


def main(args=None):
    # initiate ros2 communication
    rclpy.init(args=args)

    # create node with specified name
    node = AddTwoIntsClientNode()

    # make the node alive
    rclpy.spin(node)

    # shutdown node
    rclpy.shutdown()


if __name__ == "__main__":
    main()
