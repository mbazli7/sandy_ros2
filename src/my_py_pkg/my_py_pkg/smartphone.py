#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from example_interfaces.msg import String


class SmartphoneNode(Node):
    def __init__(self):
        super().__init__("smartphone")

        # create subscriber
        self.subscriber = self.create_subscription(
            String, "robot_news", self.robot_news_callback, 10)

        # print some info
        self.get_logger().info("Smartphone has been started")

    def robot_news_callback(self, msg: String):
        self.get_logger().info(msg.data)


def main(args=None):
    # initiate ros2 communication
    rclpy.init(args=args)

    # create node with specified name
    node = SmartphoneNode()

    # make the node alive
    rclpy.spin(node)

    # shutdown node
    rclpy.shutdown()


if __name__ == "__main__":
    main()
