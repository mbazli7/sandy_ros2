#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from example_interfaces.srv import AddTwoInts


class AddTwoIntsServerNode(Node):
    def __init__(self):
        super().__init__("add_two_ints_server")

        # create service
        self.server = self.create_service(
            AddTwoInts, "add_two_ints", self.add_two_ints_callback)

        self.get_logger().info("Add two ints server has been started.")

    def add_two_ints_callback(self, request, response):
        response.sum = request.a + request.b
        self.get_logger().info("{} + {} = {}".format(request.a, request.b, response.sum))
        return response


def main(args=None):
    # initiate ros2 communication
    rclpy.init(args=args)

    # create node with specified name
    node = AddTwoIntsServerNode()

    # make the node alive
    rclpy.spin(node)

    # shutdown node
    rclpy.shutdown()


if __name__ == "__main__":
    main()
