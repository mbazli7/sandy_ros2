#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from my_robot_interfaces.msg import HardwareStatus


class HwStatusPublisherNode(Node):
    def __init__(self):
        super().__init__("hw_status_publisher")

        # create publisher
        self.hw_status_publisher = self.create_publisher(
            HardwareStatus, "hw_status", 10)

        # create timer
        self.timer = self.create_timer(1.0, self.publish_hw_status)

        # print some info
        self.get_logger().info("HW status publisher has been started.")

    def publish_hw_status(self):
        msg = HardwareStatus()
        msg.temperature = 45
        msg.are_motors_ready = True
        msg.debug_message = "Motor ready"
        self.hw_status_publisher.publish(msg)


def main(args=None):
    # initiate ros2 communication
    rclpy.init(args=args)

    # create node with specified name
    node = HwStatusPublisherNode()

    # make the node alive
    rclpy.spin(node)

    # shutdown node
    rclpy.shutdown()


if __name__ == "__main__":
    main()
