#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from example_interfaces.msg import String


class RobotNewsStationNode(Node):
    def __init__(self):
        super().__init__("robot_news_station")

        # create publisher
        self.publisher = self.create_publisher(String, "robot_news", 10)

        # create timer
        self.timer = self.create_timer(0.5, self.publish_news)

        # print some info
        self.get_logger().info("Robot news station has been started")

    def publish_news(self):
        msg = String()
        msg.data = "Hi, this is C3PO from the robot news station."
        self.publisher.publish(msg)


def main(args=None):
    # initiate ros2 communication
    rclpy.init(args=args)

    # create node with specified name
    node = RobotNewsStationNode()

    # make the node alive
    rclpy.spin(node)

    # shutdown node
    rclpy.shutdown()


if __name__ == "__main__":
    main()
