#!/usr/bin/env python3
import rclpy
from rclpy.node import Node


class MyNode(Node):
    def __init__(self):
        super().__init__("py_test")

        # declare new parameter
        self.declare_parameter("timer_freq_sec", 0.5)
        timer_freq = self.get_parameter("timer_freq_sec").value

        self.counter = 0
        self.get_logger().info("Hello ROS2 from Python")
        self.create_timer(timer_freq, self.timer_callback)

    def timer_callback(self):
        self.counter += 1
        self.get_logger().info("timer callback {}".format(self.counter))


def main(args=None):
    # initiate ros2 communication
    rclpy.init(args=args)

    # create node with specified name
    node = MyNode()

    # make the node alive
    rclpy.spin(node)

    # shutdown node
    rclpy.shutdown()


if __name__ == "__main__":
    main()
