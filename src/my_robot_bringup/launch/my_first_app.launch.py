from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()

    py_node = Node(
        package="my_py_pkg",
        executable="py_node",
        parameters=[
            {"timer_freq_sec": 0.25}
        ]
    )

    cpp_node = Node(
        package="my_cpp_pkg",
        executable="cpp_node",
        parameters=[
            {"timer_freq_ms": 500}
        ]
    )

    ld.add_action(py_node)
    ld.add_action(cpp_node)

    return ld
