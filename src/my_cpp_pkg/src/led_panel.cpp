#include "rclcpp/rclcpp.hpp"
#include "my_robot_interfaces/msg/led_state_array.hpp"
#include "my_robot_interfaces/srv/set_led.hpp"

class LedPanelNode : public rclcpp::Node
{
private:
    rclcpp::Publisher<my_robot_interfaces::msg::LedStateArray>::SharedPtr publisher;
    rclcpp::TimerBase::SharedPtr timer;
    rclcpp::Service<my_robot_interfaces::srv::SetLed>::SharedPtr server;

    bool ledStates[3] = {false, false, false};

    void publishLedStates(void)
    {
        auto msg = my_robot_interfaces::msg::LedStateArray();
        msg.led_states = {ledStates[0], ledStates[1], ledStates[2]};
        publisher->publish(msg);
    }

    void setLedCallback(const my_robot_interfaces::srv::SetLed_Request::SharedPtr request, my_robot_interfaces::srv::SetLed_Response::SharedPtr response)
    {
        int8_t ledNumber = request->led_number;
        bool ledState = request->state;

        // set led state
        if ((ledNumber < 3) && (ledNumber >= 0))
        {
            ledStates[ledNumber] = ledState;
            response->success = true;
            // re-publish latest changes
            publishLedStates();
        }
        else
        {
            response->success = false;
        }
    }

public:
    LedPanelNode() : Node("led_panel")
    {
        // create publisher
        publisher = this->create_publisher<my_robot_interfaces::msg::LedStateArray>("led_panel", 10);

        // create timer
        timer = this->create_wall_timer(std::chrono::seconds(4),
                                        std::bind(&LedPanelNode::publishLedStates, this));

        // create service server
        server = this->create_service<my_robot_interfaces::srv::SetLed>(
            "set_led", std::bind(&LedPanelNode::setLedCallback,
                                 this, std::placeholders::_1, std::placeholders::_2));

        RCLCPP_INFO(this->get_logger(), "LED panel publisher has been started.");
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<LedPanelNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}