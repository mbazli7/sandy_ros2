#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/srv/add_two_ints.hpp"

class AddTwoIntsClientNode : public rclcpp::Node
{
private:
    std::thread thread1;
    std::thread thread2;
    std::thread thread3;

public:
    AddTwoIntsClientNode() : Node("add_two_ints_client")
    {
        RCLCPP_INFO(this->get_logger(), "Add two ints client has been started.");

        // we shall call function using thread, otherwise each callAddTwoIntsService will be blocking
        // to receive response from server, resulting signal_handler exception
        thread1 = std::thread(std::bind(&AddTwoIntsClientNode::callAddTwoIntsService, this, 10, 20));
        thread2 = std::thread(std::bind(&AddTwoIntsClientNode::callAddTwoIntsService, this, 30, 40));
        thread3 = std::thread(std::bind(&AddTwoIntsClientNode::callAddTwoIntsService, this, 50, 60));
    }

    void callAddTwoIntsService(int a, int b)
    {
        // create client and wait for server for ready
        auto client = this->create_client<example_interfaces::srv::AddTwoInts>("add_two_ints");
        while (!client->wait_for_service(std::chrono::seconds(1)))
        {
            RCLCPP_WARN(this->get_logger(), "Waiting for server Add two ints..");
        }

        // prepare the inputs
        auto request = std::make_shared<example_interfaces::srv::AddTwoInts::Request>();
        request->a = a;
        request->b = b;

        // dont wait for response from server
        auto future = client->async_send_request(request);
        try
        {
            auto response = future.get();
            RCLCPP_INFO(this->get_logger(), "%d + %d = %d", a, b, response->sum);
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), "Service call failed");
        }
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<AddTwoIntsClientNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}