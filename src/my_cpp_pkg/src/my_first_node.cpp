#include "rclcpp/rclcpp.hpp"

class MyNode : public rclcpp::Node
{
private:
    uint32_t counter = 0u;
    // create variable for timer using ROS
    rclcpp::TimerBase::SharedPtr timer;

    void timer_callback(void)
    {
        this->counter += 1u;
        RCLCPP_INFO(this->get_logger(), "timer_callback %d", this->counter);
    }

public:
    MyNode() : Node("cpp_test")
    {
        RCLCPP_INFO(this->get_logger(), "Hello ROS2 from C++");

        // declare new parameter
        this->declare_parameter("timer_freq_ms", 500);
        int64_t timer_freq = this->get_parameter("timer_freq_ms").as_int();

        // initiate the timer and bind callback
        timer = this->create_wall_timer(std::chrono::milliseconds(timer_freq),
                                        std::bind(&MyNode::timer_callback, this));
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<MyNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}