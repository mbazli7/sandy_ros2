#include "rclcpp/rclcpp.hpp"
#include "my_robot_interfaces/srv/set_led.hpp"

#include <time.h>

class BatteryNode : public rclcpp::Node
{
private:
    std::vector<std::thread> threads;
    rclcpp::TimerBase::SharedPtr timer;
    bool batteryIsEmpty = false;
    double lastTimeBatteryStateChanged = this->get_clock()->now().seconds();

    void batteryStateCallback(void)
    {
        if ((getCurrentTimeInSec() - lastTimeBatteryStateChanged) > 6)
        {
            if (batteryIsEmpty)
            {
                batteryIsEmpty = false;
                RCLCPP_INFO(this->get_logger(), "Battery full");
            }
            else
            {
                batteryIsEmpty = true;
                RCLCPP_INFO(this->get_logger(), "Battery empty");
            }

            // send msg to service to set led
            // NOTE: use vectors of thread so next operation is not deadlock with previous
            // thread (it is possible previous thread is not yet completed process)
            threads.push_back(std::thread(std::bind(&BatteryNode::callSetLedService, this, 0, batteryIsEmpty)));

            // update last time battery state changed
            lastTimeBatteryStateChanged = getCurrentTimeInSec();
        }
    }

    double getCurrentTimeInSec(void)
    {
        return this->get_clock()->now().seconds();
    }

public:
    BatteryNode() : Node("battery_node")
    {
        // create timer
        timer = this->create_wall_timer(std::chrono::milliseconds(500), std::bind(&BatteryNode::batteryStateCallback, this));

        RCLCPP_INFO(this->get_logger(), "Battery has been started.");
    }

    void callSetLedService(int8_t ledNumber, bool state)
    {
        auto client = this->create_client<my_robot_interfaces::srv::SetLed>("set_led");
        while (!client->wait_for_service(std::chrono::seconds(1)))
        {
            RCLCPP_WARN(this->get_logger(), "Waiting for server Set LED..");
        }

        // prepare the inputs
        auto request = std::make_shared<my_robot_interfaces::srv::SetLed::Request>();
        request->led_number = ledNumber;
        request->state = state;

        // dont wait for server response
        auto future = client->async_send_request(request);
        try
        {
            auto response = future.get();
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), "Service call failed");
        }
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<BatteryNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}