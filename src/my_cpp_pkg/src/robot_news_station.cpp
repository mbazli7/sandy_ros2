#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/msg/string.hpp"

class RobotNewsStationNode : public rclcpp::Node
{
private:
    // create publisher
    rclcpp::Publisher<example_interfaces::msg::String>::SharedPtr publisher;

    // create variable for timer using ROS
    rclcpp::TimerBase::SharedPtr timer;

    void publish_news(void)
    {
        auto msg = example_interfaces::msg::String();
        msg.data = "Hi, this is R2D2 from robot news station";
        publisher->publish(msg);
    }

public:
    RobotNewsStationNode() : Node("robot_news_station")
    {
        publisher = this->create_publisher<example_interfaces::msg::String>("robot_news", 10);

        // initiate the timer and bind callback
        timer = this->create_wall_timer(std::chrono::seconds(1),
                                        std::bind(&RobotNewsStationNode::publish_news, this));

        RCLCPP_INFO(this->get_logger(), "Robot news station has been started.");
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<RobotNewsStationNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}