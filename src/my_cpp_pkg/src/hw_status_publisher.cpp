#include "rclcpp/rclcpp.hpp"
#include "my_robot_interfaces/msg/hardware_status.hpp"

class HwStatusPublisherNode : public rclcpp::Node
{
private:
    // create publisher
    rclcpp::Publisher<my_robot_interfaces::msg::HardwareStatus>::SharedPtr publisher;

    // create variable for timer using ROS
    rclcpp::TimerBase::SharedPtr timer;

    void publishHwStatus(void)
    {
        auto msg = my_robot_interfaces::msg::HardwareStatus();
        msg.temperature = 45;
        msg.are_motors_ready = true;
        msg.debug_message = "Motor ready";
        publisher->publish(msg);
    }

public:
    HwStatusPublisherNode() : Node("hw_status")
    {
        publisher = this->create_publisher<my_robot_interfaces::msg::HardwareStatus>("hw_status", 10);

        // initiate the timer and bind callback
        timer = this->create_wall_timer(std::chrono::seconds(1),
                                        std::bind(&HwStatusPublisherNode::publishHwStatus, this));

        RCLCPP_INFO(this->get_logger(), "HW status publisher has been started.");
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<HwStatusPublisherNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}