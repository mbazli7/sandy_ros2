#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/msg/string.hpp"

class SmartphoneNode : public rclcpp::Node
{
private:
    // create subscriber
    rclcpp::Subscription<example_interfaces::msg::String>::SharedPtr subscriber;

    void robot_news_callback(const example_interfaces::msg::String::SharedPtr msg)
    {
        RCLCPP_INFO(this->get_logger(), "%s", msg->data.c_str());
    }

public:
    SmartphoneNode() : Node("smartphone")
    {
        subscriber = this->create_subscription<example_interfaces::msg::String>(
            "robot_news", 10,
            std::bind(&SmartphoneNode::robot_news_callback, this, std::placeholders::_1));

        // print some info
        RCLCPP_INFO(this->get_logger(), "Smartphone has been started");
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<SmartphoneNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}