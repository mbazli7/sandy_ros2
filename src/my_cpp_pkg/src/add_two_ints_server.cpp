#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/srv/add_two_ints.hpp"

class AddTwoIntsServerNode : public rclcpp::Node
{
private:
    rclcpp::Service<example_interfaces::srv::AddTwoInts>::SharedPtr server;

    void addTwoIntsCallback(const example_interfaces::srv::AddTwoInts_Request::SharedPtr request, const example_interfaces::srv::AddTwoInts_Response::SharedPtr response)
    {
        response->sum = request->a + request->b;
        RCLCPP_INFO(this->get_logger(), "%d + %d = %d", request->a, request->b, response->sum);
    }

public:
    AddTwoIntsServerNode() : Node("add_two_ints_server")
    {
        // initialize service server
        server = this->create_service<example_interfaces::srv::AddTwoInts>(
            "add_two_ints", std::bind(&AddTwoIntsServerNode::addTwoIntsCallback,
                                      this, std::placeholders::_1, std::placeholders::_2));
        RCLCPP_INFO(this->get_logger(), "Add two ints server has been started.");
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<AddTwoIntsServerNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}