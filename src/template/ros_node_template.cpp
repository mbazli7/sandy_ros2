#include "rclcpp/rclcpp.hpp"

class MyCustomNode : public rclcpp::Node
{
private:
public:
    MyCustomNode() : Node("node_name")
    {
        RCLCPP_INFO(this->get_logger(), "Hello ROS2 from C++");
    }
};

int main(int argc, char **argv)
{
    // initiate ros communication
    rclcpp::init(argc, argv);

    // create node with shared pointer
    auto node = std::make_shared<MyCustomNode>();

    // make the node alive
    rclcpp::spin(node);

    // shutdown node
    rclcpp::shutdown();

    return 0;
}