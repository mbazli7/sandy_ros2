#!/usr/bin/env python3
import rclpy
from rclpy.node import Node


class MyCustomNode(Node):
    def __init__(self):
        super().__init__("node_name")
        self.get_logger().info("Hello ROS2 from Python")


def main(args=None):
    # initiate ros2 communication
    rclpy.init(args=args)

    # create node with specified name
    node = MyCustomNode()

    # make the node alive
    rclpy.spin(node)

    # shutdown node
    rclpy.shutdown()


if __name__ == "__main__":
    main()
