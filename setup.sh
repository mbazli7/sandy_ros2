#!/bin/bash

# ROS: source ROS setup script
source "/home/mbazli/ros2_foxy/ros2-linux/setup.bash"

# ROS: source ROS workspace (need to be done for every specific project)
source "/home/mbazli/Documents/software-house/ros/code/sandy_ros2/install/setup.bash"